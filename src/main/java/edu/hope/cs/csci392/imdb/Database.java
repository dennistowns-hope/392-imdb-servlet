package edu.hope.cs.csci392.imdb;

import java.util.ArrayList;
import java.util.List;

import edu.hope.cs.csci392.imdb.model.Actor;
import edu.hope.cs.csci392.imdb.model.Movie;
import edu.hope.cs.csci392.imdb.model.Role;

public class Database {
	private static Database instance;
	
	Actor hanks = new Actor (
		55015, "Tom", "Hanks", "Concord", "CA",
		null, null, 'M', 71
	);
	
	Actor ryan = new Actor (
		222625, "Meg", "Ryan", "Fairfield", "CT",
		null, null, 'F', 68
	);
	
	Movie sleepless = new Movie (
		207627, "Sleepless in Seattle", 1993, 102, "Comedy"
	);
	
	Movie mail = new Movie (
			254549, "You've got mail", 1998, 119, "Comedy"
	);
	
	Role hanksSleepless = new Role (
		hanks.getActorID(), sleepless.getMovieID(), 
		"Samuel 'Sam' Baldwin"
	);

	Role hanksMail = new Role (
		hanks.getActorID(), mail.getMovieID(), 
		"Joe Fox"
	);
	
	Role ryanSleepless = new Role (
		ryan.getActorID(), sleepless.getMovieID(),
		"Annie Reed"
	);
	
	Role ryanMail = new Role (
		ryan.getActorID(), mail.getMovieID(),
		"Kathleen Kelly"
	);
	
	private Database ()
	{			
	}
	
	public static Database getInstance () {
		if (instance == null) {
			instance = new Database ();
		}
		
		return instance;
	}
	
	/**
	 * Finds the actors that match the given conditions.  Only those 
	 * parameters that have non-empty values are included in the query.  
	 * Those parameters that are included are combined using AND.
	 * @param stageFirstName the stage first name of the actor(s) to locate
	 * @param stageLastName the stage last name of the actor(s) to locate
	 * @param birthCity the city where the actor(s) to locate were born
	 * @param birthStateOrProvince the state where the actor(s) to locate were born
	 * @return a List of Actor objects representing the actor(s) matching the
	 * specified conditions
	 */
	public List<Actor> findActors (
		String stageFirstName, String stageLastName,
		String birthCity, String birthStateOrProvince
	) 
	{		
		ArrayList<Actor> actors = new ArrayList<Actor> ();
		actors.add (hanks);
		actors.add (ryan);
		return actors;		
	}
	
	/**
	 * Finds the movies that match the given conditions.  All conditions
	 * are combined using AND.
	 * @param movieTitle the title of the movie(s) to be located, or an
	 * empty string if title should not be included as part of the criteria
	 * @param year the year the movie was made, or -1 if year should not be
	 * included as part of the criteria
	 * @param runningTimeComparator a string indicating the type of comparison
	 * to be used for the running time.  This string can be used directly in
	 * the SQL query
	 * @param runningTimeValue the running time of the movie(s) to be located,
	 * or -1 if running time should not be included as part of the criteria
	 * @param primaryGenre the primary genre of the movie(s) to be located,
	 * or an empty string if the primary genre should not be included as part
	 * of the criteria
	 * @param director the director of the movie(s) to be located, or an empty string
	 * if the director should not be included as part of the criteria
	 * @return a List of Movie objects representing the movie(s) matching the
	 * specified conditions
	 */
	public List<Movie> findMovies (
		String movieTitle, int year, String runningTimeComparator, 
		int runningTimeValue, String primaryGenre, String director
	) {
		ArrayList<Movie> movies = new ArrayList<Movie> ();
		movies.add (sleepless);
		movies.add (mail);
		return movies;
	}
	
	/**
	 * Finds all the roles played by an actor.
	 * @param actor the actor to search for
	 * @return a List of Role objects representing the roles played by the
	 * specified actor
	 */
	public List<Role> findMoviesForActor (Actor actor) {
		ArrayList<Role> roles = new ArrayList<Role> ();
		
		if (actor.getActorID() == hanks.getActorID()) {
			roles.add (hanksSleepless);
			roles.add (hanksMail);
		}
		
		if (actor.getActorID() == ryan.getActorID()) {
			roles.add (ryanSleepless);
			roles.add (ryanMail);
		}				
		return roles;
	}
	
	/**
	 * Finds the cast of a given movie
	 * @param movie the movie to search for
	 * @return a List of Role objects representing the roles in the
	 * specified movie
	 */
	public List<Role> findRolesInMovie (Movie movie) {
		ArrayList<Role> roles = new ArrayList<Role> ();
		if (movie.getMovieID() == sleepless.getMovieID()) {
			roles.add (hanksSleepless);
			roles.add (ryanSleepless);
		}
		
		if (movie.getMovieID() == mail.getMovieID()) {
			roles.add (hanksMail);
			roles.add (ryanMail);
		}
		
		return roles;
	}
	
	/**
	 * Finds a given actor based on Actor ID
	 * @param actorID the ID to search for
	 * @return the actor with the given ID, or null if none can be found
	 */
	public Actor findActorByID (int actorID) {
		if (actorID == hanks.getActorID()) {
			return hanks;
		}
		
		if (actorID == ryan.getActorID()) {
			return ryan;
		}
		
		return null;
	}
	
	/**
	 * Finds a given movie based on Movie ID
	 * @param movieID the ID to search for
	 * @return the movie with the given ID, or null if none can be found
	 */
	public Movie findMovieByID (int movieID) {
		if (movieID == sleepless.getMovieID()) {
			return sleepless;
		}
		
		if (movieID == mail.getMovieID()) {
			return mail;
		}
		
		return null;
	}
	
	public List<String> findGenres () {
		ArrayList<String> genres = new ArrayList<String> ();
		genres.add("Comedy");
		genres.add("Drama");
		genres.add("Romance");
		genres.add("War");
		
		return genres;
	}
}
